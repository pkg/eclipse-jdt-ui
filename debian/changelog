eclipse-jdt-ui (4.26-1+apertis1) apertis; urgency=medium

  * Move package to development repository. Needed for the Java suite

 -- Ritesh Raj Sarraf <ritesh.sarraf@collabora.com>  Tue, 10 Oct 2023 21:12:05 +0530

eclipse-jdt-ui (4.26-1+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 05 Oct 2023 16:23:48 +0000

eclipse-jdt-ui (4.26-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-jdt-core-java (>= 3.32)
    - Build more bundles with Java 11
  * Standards-Version updated to 4.6.2

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 05 Jan 2023 12:19:29 +0100

eclipse-jdt-ui (4.23-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-core-resources-java (>= 3.16)
    - Depend on libeclipse-jface-text-java (>= 3.20)
    - Build org.eclipse.jdt.ui with Java 11
  * Standards-Version updated to 4.6.1
  * Track and download the new releases from GitHub

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 09 Dec 2022 09:32:38 +0100

eclipse-jdt-ui (4.21-1) unstable; urgency=medium

  * New upstream release

 -- Emmanuel Bourg <ebourg@apache.org>  Sun, 24 Oct 2021 23:38:35 +0200

eclipse-jdt-ui (4.18-1) unstable; urgency=medium

  * New upstream release
  * Standards-Version updated to 4.6.0.1
  * Switch to debhelper level 13
  * Track the new releases from git.eclipse.org (the GitHub repository is gone)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 18 Oct 2021 11:40:05 +0200

eclipse-jdt-ui (4.15-1+apertis1) apertis; urgency=medium

  * Set component to sdk.

 -- Vignesh Raman <vignesh.raman@collabora.com>  Thu, 06 Jan 2022 17:24:54 +0530

eclipse-jdt-ui (4.15-1apertis0) apertis; urgency=medium

  * Sync from Debian debian/bullseye.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 11 Mar 2021 21:10:31 +0000

eclipse-jdt-ui (4.15-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-jdt-core-java (>= 3.20)
    - Depend on libeclipse-jface-text-java (>= 3.16)
    - Updated the bundle dependencies
  * Standards-Version updated to 4.5.0
  * Switch to debhelper level 12

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 07 May 2020 23:39:22 +0200

eclipse-jdt-ui (4.12-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-jdt-core-java (>= 3.18)
  * Standards-Version updated to 4.4.0

 -- Emmanuel Bourg <ebourg@apache.org>  Tue, 06 Aug 2019 09:14:34 +0200

eclipse-jdt-ui (4.11-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-ui-ide-java (>= 3.15)

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 11 Jul 2019 17:57:00 +0200

eclipse-jdt-ui (4.10-1co1) apertis; urgency=medium

  [ Ritesh Raj Sarraf ]
  * debian/apertis/component: Set to development

 -- Emanuele Aina <emanuele.aina@collabora.com>  Mon, 15 Feb 2021 23:25:53 +0000

eclipse-jdt-ui (4.10-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-jdt-core-java (>= 3.16)
    - Depend on libeclipse-jface-text-java (>= 3.15)
    - Depend on libeclipse-text-java (>= 3.8)
    - Updated the bundle dependencies
  * Standards-Version updated to 4.3.0

 -- Emmanuel Bourg <ebourg@apache.org>  Fri, 28 Dec 2018 15:32:30 +0100

eclipse-jdt-ui (4.9-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-jdt-core-java (>= 3.15)
    - Depend on libeclipse-jdt-launching-java (>= 3.11)
    - The license changed to EPL-2.0

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 10 Dec 2018 22:54:03 +0100

eclipse-jdt-ui (4.8-1) unstable; urgency=medium

  * New upstream release
    - Depend on libeclipse-jdt-core-java (>= 3.14)
    - Depend on libeclipse-jdt-launching-java (>= 3.10)
    - Depend on libeclipse-jface-java (>= 3.14)
    - Depend on libeclipse-jface-text-java (>= 3.13)
    - Updated the bundle dependencies

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 28 Nov 2018 12:58:20 +0100

eclipse-jdt-ui (4.7.3-2) unstable; urgency=medium

  * Build more bundles:
    - Build the org.eclipse.jdt.astview bundle
    - Build the org.eclipse.jdt.ui bundle
  * Fixed the watch file

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 22 Nov 2018 18:31:10 +0100

eclipse-jdt-ui (4.7.3-1) unstable; urgency=medium

  * Initial release (Closes: #912257)

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 29 Oct 2018 17:58:35 +0100
